const initialState = {
    code: '',
    currentCode: '9966',
    check: '',
    codeMess: '',
    stars: ''
};

const reducer = (state = initialState, action) => {
    if (action.type === "DELETE_CODE") {
        const newCode = state.code.substr (0, state.code.length - 1);
        const newStars = state.stars.substr (0, state.stars.length - 1);
        if (state.code.length === 1) {
            return {
                ...state,
                codeMess: '',
                check: '',
                code: newCode,
                stars: newStars
            }
        }
        return {
            ...state,
            code: newCode,
            stars: newStars
        }
    }

    if (action.type === "ADD_CODE") {
        if (state.code.length < 4) {
            return {
                ...state,
                code: state.code + action.number,
                stars: state.stars + '*'
            }
        }
    }

    if (action.type === "CHECK_CODE") {
        if (state.code === state.currentCode) {
            return {
                ...state,
                check: 'success',
                codeMess: 'Access is allowed'
            }
        } else {
            return {
                ...state,
                check: 'error',
                codeMess: 'Access is denied'
            }
        }
    }

    return state;
};


export default reducer;
