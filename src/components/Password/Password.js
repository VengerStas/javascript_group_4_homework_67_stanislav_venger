import React, {Component} from 'react';
import './Password.css';
import {connect} from "react-redux";


class Password extends Component {
    render() {
        return (
            <div className="interface">
                <div className={"pass-display ".concat(this.props.check)}>
                    <h1 className="pass">{this.props.stars}</h1>
                    <p className="access-txt">{this.props.codeMess}</p>
                </div>
                <div className="keyboard">
                    <button className="btn" onClick={() => this.props.addCode(7)}>7</button>
                    <button className="btn" onClick={() => this.props.addCode(8)}>8</button>
                    <button className="btn" onClick={() => this.props.addCode(9)}>9</button>
                    <button className="btn" onClick={() => this.props.addCode(4)}>4</button>
                    <button className="btn" onClick={() => this.props.addCode(5)}>5</button>
                    <button className="btn" onClick={() => this.props.addCode(6)}>6</button>
                    <button className="btn" onClick={() => this.props.addCode(1)}>1</button>
                    <button className="btn" onClick={() => this.props.addCode(2)}>2</button>
                    <button className="btn" onClick={() => this.props.addCode(3)}>3</button>
                    <button className="btn" onClick={this.props.deleteCode}>&lt;</button>
                    <button className="btn" onClick={() => this.props.addCode(0)}>0</button>
                    <button className="btn" onClick={this.props.checkCode}>E</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        stars: state.stars,
        check: state.check,
        codeMess: state.codeMess
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addCode: number => dispatch({type: 'ADD_CODE', number}),
        deleteCode: () => dispatch({type: 'DELETE_CODE'}),
        checkCode: () => dispatch({type: 'CHECK_CODE'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);
